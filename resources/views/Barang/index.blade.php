@extends('layouts.app')

@section('content')

<a href="barang/create" class="btn btn-success" style="margin-left: 1100px">Create New Barang</a>
<br>
<table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Kategori</th>
                    <th>Stock</th>
                    <th>Action</th>
                  </tr>
                </thead>
                @foreach($p as $key => $t)
                <tbody>
                  <tr>
                    <td>{{$key+1}}</td>                   
                    <td>{{$t->name}}</td>                   
                    <td>Rp {{$t->harga}}</td>
                    <td>{{$t->kategori}}</td>                   
                    <td>{{$t->stok}}</td>                   
                    <td>
                      <div class="btn-group">
                        <a href="{{URL('barang/show/'.$t->id)}}" class="btn btn-primary">Show</a>
                        <a onclick="return confirm('Do You Want To Edit Items {{$t->title}} ??')" href="{{URL('barang/edit/'.$t->id)}}" class="btn btn-warning">Edit</a>
                        <a onclick="return confirm('Do You Want To Delete Items {{$t->title}} ??')" href="{{URL('barang/destroy/'.$t->id)}}" class="btn btn-danger">⛔Delete</a>
                      </div>
                    </td>
                  </tr>
                </tbody>
                @endforeach
              </table>

@endsection