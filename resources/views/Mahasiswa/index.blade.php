@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Mahasiswa Baru</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="create/mhsw"> Create New Mahasiswa</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>Id</th>
            <th>Nisn</th>
            <th>Nama Lengkap</th>
            <th>Jenis Kelamin</th>
            <th>Alamat</th>
            <th>Provinsi</th>
            <th>Kabupaten</th>
            <th>Asal Sekolah</th>
            <th>Nilai Rata UN</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($mahasiswa as $msw)
        <tr>
            <td>{{ $msw->id }}</td>
            <td>{{ $msw->nisn }}</td>
            <td>{{ $msw->nama_depan}} {{$msw->nama_belakang }}</td>
            <td>{{ $msw->jk }}</td>
            <td>{{ $msw->alamat }}</td>
            <td>{{ $msw->provinsi }}</td>
            <td>{{ $msw->kabupaten }}</td>
            <td>{{ $msw->asal_sekolah }}</td>
            <td>{{ $msw->nilai_rata }}</td>
            <td>
               <div class="btn-group">
                 <a href="{{URL('show/'.$msw->id)}}" class="btn btn-info">View</a>
                 <a onclick="return confirm('Do You Want To Edit Menu {{$msw->nama_depan}}??')" href="{{URL('mahasiswa/edit/'.$msw->id)}}" class="btn btn-warning">Edit</a>
                 <a onclick="return confirm('Do You Want To Delete Menu {{$msw->nama_depan}}??')" href="{{URL('mahasiswa/destroy/'.$msw->id)}}" class="btn btn-danger">⛔Delete</a>
               </div>
            </td>
        </tr>
        @endforeach
    </table>      
@endsection