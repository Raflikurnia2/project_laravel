@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <a href="products" class="btn btn-warning" style="margin-left: 50px">Product</a>
                    <a href="mahasiswa" class="btn btn-success" style="margin-left: 70px">Mahasiswa</a>
                    <a href="items" class="btn btn-primary" style="margin-left: 70px">Items</a>
                    <a href="barang" class="btn btn-danger" style="margin-left: 70px">Barang</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
